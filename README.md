# MNT Reform Layerscape LS1028A SoM

![MNT Reform Layerscape LS1028A SoM Render](images/reform-ls1028a-render2.png)

This is the open hardware system-on-module featuring the NXP Layerscape LS1028A processor and 16GB of DDR4 RAM. It can be used in the [MNT Reform open hardware laptop](https://mntre.com/reform) or in any other custom device.

The module was designed by [RBZ Embedded Logics](https://www.rbz.es/) in collaboration with MNT Research.

## Status

The first production version of the module (rev02b, see below) runs Linux and is fully validated and integrated with MNT Reform.

The design is made in Altium. The free and open source EDA suite KiCAD version 6 can import these files for inspection and editing.

You can manufacture the module yourself or acquire it here:

- Module: https://shop.mntre.com/products/mnt-reform-ls1028a-module
- Installed in MNT Reform OSHW laptop: https://shop.mntre.com/products/mnt-reform

## Specs

- System-on-Chip: [NXP LS1028A](https://www.nxp.com/products/processors-and-microcontrollers/arm-processors/layerscape-processors/layerscape-1028a-applications-processor:LS1028A)
- CPU: 2x ARM Cortex-A72 (1.5GHz, 1MB L2, 48KB L1I, 32KB L1D, ECC, Cache Coherent Interconnect CCI-400)
- GPU: Vivante GC7000L (open source etnaviv drivers in Linux kernel), ARM eDP Controller
- Memory: 16GB DDR4
- 2x PCIe 3.0 (1x external PCIe 3.0 routed to HDMI pins)
- SATA 3
- 2x USB 3.0
- 1GBit Ethernet w/ Marvell Alaska PHY
- SAI audio
- eMMC (size TBD), QSPI flash, EEPROM
- SD, UART, SPI, I2C, PWM, GPIO
- Powered by single 5V input

![MNT Reform Layerscape LS1028A SoM Schematics Overview](images/mnt-reform-ls1028a-som-schematics.png)

## Firmware/Software

- For sources, see `mkimage.sh`: https://source.mnt.re/reform/reform-system-image
- Ready-to-use Debian GNU/Linux images: http://mnt.re/system-image

## Rev02b Changes

- The 9 level shifters (NXB0104BQX) had the wrong footprints. This has been corrected.
- The intented BOOTCFG mechanism via level shifter D6 does not work, so we remove it in production and connect the three `CFG_RCW_SRC0..2` pins to GND to select SD card boot.

## Rev01 Changes

Schematics:

- 2x 49R9 pull-down resistors added in the outputs of G5, before  decoupling caps.
- Level shifting voltage translators ref. TXS0108ERGYR changed to NXB0104BQX.
- NXB0104BQX voltage translators added to isolate CFG_RCW_SRC[2..0] from UART1.TX and UART2.TX in boot-up procedure.
- D12 power rail connected to 3V3, instead of 1V2.
- TA_TMP_DETECT pin connected to +VDD.
- I2C voltage translator added to I2C1.

PCB:

- DDR4 footprint modified to match 10.5mm x 13mm x 1.2mm dimensions.
- Manufacturing notes added to the PCB.

## Copyright

All hardware design work in this repository is © 2021 [RBZ Embedded Logics / RBZ Robot Design S.L.](https://www.rbz.es/), Madrid, Spain. The module was designed in collaboration with [MNT Research GmbH](https://mntre.com), Berlin, Germany, who are hosting the repository.

## License

All hardware sources are licensed under the [CERN Open Hardware Licence Version 2 - Strongly Reciprocal](https://ohwr.org/project/cernohl/wikis/uploads/002d0b7d5066e6b3829168730237bddb/cern_ohl_s_v2.txt).

## Funding

![NLNet logo](images/nlnet-320x120.png)

This project [receives funding by NLNet](https://nlnet.nl/project/MNT-Reform/).
